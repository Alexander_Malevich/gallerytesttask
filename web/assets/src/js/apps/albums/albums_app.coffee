
PhotoGallery.module 'GalleryApp', (GalleryApp, PhotoGallery, Backbone, Marionette, $, _) ->

    GalleryApp.Router = Marionette.AppRouter.extend
        appRoutes:
            'albums': 'listAlbums'
            'albums/:id': 'showAlbum'
            'albums/:id/page/:page': 'showAlbum'

    API =
        listAlbums: ->
            PhotoGallery.GalleryApp.List.Controller.listAlbums()
            undefined
        showAlbum: (id, page = '1') ->
            PhotoGallery.GalleryApp.Show.Controller.showAlbum id, page
            undefined


    PhotoGallery.on 'albums:list', ->
        PhotoGallery.navigate 'albums'
        API.listAlbums()
        undefined

    PhotoGallery.on 'album:show', (id) ->
        PhotoGallery.navigate 'albums/' + id
        API.showAlbum id
        undefined

    PhotoGallery.on 'before:start', ->
        new GalleryApp.Router
            'controller': API
        undefined

    undefined