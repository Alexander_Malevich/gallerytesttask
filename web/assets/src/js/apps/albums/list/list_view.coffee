
PhotoGallery.module 'GalleryApp.List', (List, PhotoGallery, Backbone, Marionette, $, _) ->
    List.Album = Marionette.ItemView.extend
        tagName: 'tr'
        template: '#album-list-item'
        events:
            'click': 'highlightName'
            'click td a.js-show': 'showClicked'
        highlightName: (e) ->
            e.preventDefault()
            @$el.toggleClass 'warning'
            undefined
        showClicked: (e) ->
            e.preventDefault()
            @trigger 'album:show', @model
            undefined


    List.Albums = Marionette.CompositeView.extend
        tagName: 'table'
        className: 'table table-hover'
        template: '#album-list'
        childView: List.Album
        childViewContainer: 'tbody'

    undefined

