
PhotoGallery.module 'GalleryApp.Show', (Show, PhotoGallery, Backbone, Marionette, $, _) ->

    Show.Controller =
        showAlbum: (id, page) ->
            loadingView = new PhotoGallery.Common.Views.Loading()
            PhotoGallery.mainRegion.show loadingView

            promise = PhotoGallery.request 'album:entity', id, page
            promise.then (album) ->
                if album != undefined
                    albumShowLayout = new Show.Layout()
                    albumShowItem = new Show.Album
                        model: album
                    albumShowInfo = new Show.Info
                        model: album
                    albumShowPagination = new Show.Pagination()

                    albumShowLayout.on 'show', ->
                        pagination = $.get "/api/albums/#{album.id}/pagination", (data) ->
                            $('#pagination-region').html data
                            $('.pagination > span:first').html('<a href="#albums/#{album.id}/page/1">1</a>')
                            $('.pagination > span > a').each ->
                                $(@).attr 'href', "#albums/#{album.id}/page/#{$(@).text()}"
                                if $(@).text() == album.attributes.page
                                    $(@).addClass('disabled')
                                undefined
                            $('.pagination > span').removeClass('current').addClass('page')
                            $('.pagination > span.next, .pagination > span.last').remove()
                            undefined
                        albumShowLayout.infoRegion.show albumShowInfo
                        albumShowLayout.imagesRegion.show albumShowItem
                        albumShowLayout.paginationRegion.show albumShowPagination
                        undefined
                else
                    albumShowLayout = new Show.MissingAlbum()

                PhotoGallery.mainRegion.show albumShowLayout
                undefined

            undefined

    undefined

