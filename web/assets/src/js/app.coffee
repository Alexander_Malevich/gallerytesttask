

PhotoGallery = new Marionette.Application();

PhotoGallery.addRegions
  mainRegion: '#main-region'


PhotoGallery.navigate = (route, options = {}) ->
  Backbone.history.navigate route, options
  undefined


PhotoGallery.getCurrentRoute = ->
  Backbone.history.fragment

PhotoGallery.on 'start', ->
  if Backbone.history
    Backbone.history.start()
    if @getCurrentRoute() == ''
      PhotoGallery.trigger 'albums:list'
  undefined

