<?php

namespace GalleryBundle\Controller;

use Doctrine\ORM\QueryBuilder;
use GalleryBundle\Entity\Album;
use GalleryBundle\Entity\Image;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use GalleryBundle\Repository\AlbumRepository;


class AlbumController extends Controller
{

    /**
     * @Route("/api/albums")
     * @Method("GET")
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()->getRepository('GalleryBundle:Album');
        //AJAX get albums with max 10 images//
        /*$albums = $repository->getAlbumsByImagesAmount(10);*/
        $albums = $repository->findAll();
        $albumManager = $this->get('gallery.album');
        //\Doctrine\Common\Util\Debug::dump($albumManager->serializeToJSON($albums[0]));
        return Response::create($albumManager->serializeToJSON($albums));
    }

    /**
     * @Route("/api/albums/{id}/page/{page}")
     * @Method("GET")
     */
    public function showAction($id, $page = 1)
    {
        $album = $this->getDoctrine()->getRepository('GalleryBundle:Album')->getPaginatedAlbumImages($id, $page);
        $albumManager = $this->get('gallery.album');

        return Response::create($albumManager->serializeToJSON($album));
    }

    /**
     * @Route("/api/albums/{id}/pagination")
     * @Method("GET")
     */
    public function paginationAction($id)
    {
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $this->getDoctrine()->getRepository('GalleryBundle:Album')->getAllAlbumImages($id),
            1,
            10
        );

        return $this->render('GalleryBundle:Album:pagination.html.twig', ['pagination' => $pagination]);
    }

}
