<?php

/**
 * Created by PhpStorm.
 * User: Alexander_Malevich
 * Date: 01.09.2016
 * Time: 10:48
 */
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use GalleryBundle\Entity\Album;
use GalleryBundle\Entity\Image;

class LoadAlbumsData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for ($albums_counter = 1; $albums_counter <= 5; $albums_counter++) {
            $album = new Album();
            $album->setTitle("Album $albums_counter");
            $album->setDescription("Album $albums_counter description");
            $album->setCreatedAt(new \DateTime("now"));

            for ($images_counter = 1; $images_counter <= ($albums_counter === 1 ? 5 : mt_rand(20, 25)); $images_counter++) {
                $image = new Image();
                $image->setTitle("Image $images_counter");
                $image->setPath("/uploads/fixtures/images/albums/$albums_counter/$images_counter.jpg");
                $image->setCreatedAt(new \DateTime());
                $image->setUpdatedAt(new \DateTime());
                $image->setAlbum($album);

                $manager->persist($image);
                $album->addImage($image);
            }

            $manager->persist($album);
            $manager->flush();
        }
    }
}